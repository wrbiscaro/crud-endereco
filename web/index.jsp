<%-- 
    Document   : index
    Created on : 24/05/2016, 13:25:51
    Author     : Wallace Biscaro
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="br.com.crudendereco.controller.Conexao"%>
<%@page import="br.com.crudendereco.model.Endereco"%>
<%@page import="br.com.crudendereco.controller.EnderecoDAO"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Conexao conexao = new Conexao();
    EnderecoDAO enderecoDao = new EnderecoDAO(conexao.conectar());
    ArrayList<Endereco> enderecos = new ArrayList<>();
    
    if(request.getParameter("id") != null)
    {
        request.setAttribute("id", request.getParameter("id"));
        
        Endereco e = enderecoDao.selecionar(Integer.parseInt(request.getParameter("id")));
        enderecos.add(e);
    }
    else
        enderecos = enderecoDao.listar();
    
    //Add no escopo de requisicao para ser acessado por EL
    request.setAttribute("enderecos", enderecos);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>CRUD Endereço</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <legend>Endereços cadastrados</legend>
                    <div id="div_msg" style="text-align:center;color: red;">
                        ${msg}
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="col-md-6">
                                <div class="form-group">
                                    <label for="id">ID do endereço:</label>
                                    <input id="id" class="form-control" type="text" autofocus="" name="id" value="${id}">                           
                                </div>
                            </fieldset>
                            <div class="col-md-3">
                                <br/>
                                <button class="btn btn-primary btn-lg" type="button" onclick="buscar_endereco();">
                                    <span class="glyphicon glyphicon-search"></span> 
                                    Localizar
                                </button>
                            </div>
                            <div class="col-md-3">
                                <br/>
                                <a href="index.jsp"class="btn btn-primary btn-lg">
                                    <span class="glyphicon glyphicon-remove"></span> 
                                    Limpar
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6" align="right">
                            <br/>
                            <a href="inserir_endereco.jsp" class="btn btn-primary btn-lg">
                                <span class="glyphicon glyphicon-plus"></span>
                                Inserir
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <c:if test="${not empty enderecos}">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" align="center">
                                        <thead>
                                            <tr style="text-align: center;">
                                                <th>ID</th>
                                                <th>Rua</th>
                                                <th>Número</th>
                                                <th>CEP</th>
                                                <th>Cidade</th>
                                                <th>Estado</th>
                                                <th>Bairro</th>
                                                <th>Complemento</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="e" items="${enderecos}">
                                                <tr style="text-align: center;">
                                                    <td>${e.id}</td>
                                                    <td>${e.rua}</td>
                                                    <td>${e.numero}</td>
                                                    <td>${e.cep}</td>
                                                    <td>${e.cidade}</td>
                                                    <td>${e.estado}</td>
                                                    <td>${e.bairro}</td>
                                                    <td>${e.complemento}</td>
                                                    <td><a href="alterar_endereco.jsp?id=${e.id}" class="btn btn-primary">Alterar</a>
                                                        <input type="button" class="btn btn-danger" id="excluir" name="excluir" onclick="excluirEndereco(${e.id});" value="Excluir">
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>                                
                            </c:if>
                            <c:if test="${empty enderecos}">
                                <br/>
                                <p align="center"><b>Não há endereços cadastrados! Para cadastrar, clique no botão "Inserir" no canto superior direito.</b></p>
                            </c:if>                               
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.maskedinput.js"></script>
    </body>
</html>

<script type="text/javascript">
    function buscar_endereco()
    {
        var id = $("#id").val();
        
        if($.isNumeric(id) && id % 1 === 0)
        {
            $.ajax({
                type: "POST",
                url: "Servlet",
                data: "classe=AjaxEndereco&acao=localizar&id=" + id,
                success: function(msg)
                {
                    if(msg === "erro")                  
                        alert("Endereço não encontrado!");
                    else
                        location.href = "index.jsp?id=" + id;
                }
            }); 
        }
        else
            alert("ID inválido!");
    }
    
    function excluirEndereco(id)
    {
        if(confirm("Deseja realmente excluir este endereço?"))
        {
            $.ajax({
                type: "POST",
                url: "Servlet",
                data: "classe=AjaxEndereco&acao=excluir&id=" + id,
                success: function(msg){	
                    alert(msg);
                    location.reload();
                }
            }); 
        }
    }
</script>
