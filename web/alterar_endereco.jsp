<%-- 
    Document   : alterar_endereco
    Created on : 26/05/2016, 00:24:49
    Author     : Wallace Biscaro
--%>

<%@page import="br.com.crudendereco.controller.Conexao"%>
<%@page import="br.com.crudendereco.model.Endereco"%>
<%@page import="br.com.crudendereco.controller.EnderecoDAO"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    if(request.getAttribute("e") == null && request.getParameter("id") != null) 
    {
        Conexao conexao = new Conexao();
        EnderecoDAO enderecoDao = new EnderecoDAO(conexao.conectar());
        Endereco e = enderecoDao.selecionar(Integer.parseInt(request.getParameter("id")));
        request.setAttribute("e", e);    
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>CRUD Endereço</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-lg-2"></div>
                <div class="col-sm-8 col-lg-8">
                    <legend>Alterar endereço</legend>
                    
                    <div id="div_msg" style="color: red; padding: 10px;">
                        <b>${msg}</b>
                    </div> 
                    <form class="col-sm-12 col-lg-12" action="Servlet" method="POST" onsubmit="return validar();">
                        <input type="hidden" name="classe" value="LogicaEndereco" />
                        <input type="hidden" name="Endereco" value="Alterar" readonly="readonly" />  
                        <input type="hidden" name="id" value="${e.id}" readonly="readonly" />
                        
                        <div class="row">
                            <fieldset class="col-md-6">
                                <div class="form-group">
                                    <label for="cep">CEP</label>
                                    <input id="cep" class="form-control" type="text" required="required" autofocus="" name="cep" value="${e.cep}">                           
                                </div>
                            </fieldset>
                            <div class="col-md-6">
                                <br/>
                                <button class="btn btn-primary btn-lg" type="button" onclick="buscar_cep();">
                                    <span class="glyphicon glyphicon-search"></span> 
                                    Buscar
                                </button>
                            </div>
                        </div>

                        <div id="resultado" style="color: red;"></div><br/>

                        <div class="row" id="form">
                            <fieldset class="col-md-6">
                                <div class="form-group">
                                    <label for="rua">Rua</label>
                                    <input id="rua" class="form-control" type="text" name="rua" value="${e.rua}">
                                </div>
                            </fieldset>
                            <fieldset class="col-md-6">
                                <div class="form-group">
                                    <label for="bairro">Bairro</label>
                                    <input id="bairro" class="form-control" type="text" name="bairro" value="${e.bairro}">
                                </div>
                            </fieldset>
                            <fieldset class="col-md-6">
                                <div class="form-group">
                                    <label for="cidade">Cidade</label>
                                    <input id="cidade" class="form-control" type="text" name="cidade" value="${e.cidade}">
                                </div>
                            </fieldset>
                            <fieldset class="col-md-6">
                                <div class="form-group">
                                    <label for="estado">Estado</label>
                                    <input id="estado" class="form-control" type="text" name="estado" value="${e.estado}">
                                </div>
                            </fieldset>
                            <fieldset class="col-md-6">
                                <div class="form-group">
                                    <label for="cidade">Número</label>
                                    <input id="numero" class="form-control" type="text" name="numero" value="${e.numero}">
                                </div>
                            </fieldset>
                            <fieldset class="col-md-6">
                                <div class="form-group">
                                    <label for="estado">Complemento</label>
                                    <input id="complemento" class="form-control" type="text" name="complemento" value="${e.complemento}">
                                </div>
                            </fieldset> 
                            
                            <center>
                                <button id="calcular" class="btn btn-primary btn-lg" type="submit">
                                    <span class="glyphicon glyphicon-edit"></span> 
                                    Alterar
                                </button> 
                                <a href="index.jsp" class="btn btn-primary btn-lg">
                                    <span class="glyphicon glyphicon-backward"></span>
                                    Voltar
                                </a>
                            </center>
                        </div>

                        <div class="row" id="loading" style="display:none;"><center><img src="img/loading.gif"></center></div>
                    </form>
                </div>               
                <div class="col-sm-2 col-lg-2"></div>
            </div>
        </div>
        
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.maskedinput.js"></script>
    </body>
</html>

<script type="text/javascript">
    $(document).ready(function(){
        $("#cep").mask("99999-999");
    });

    function preenche_form(cep) {  
        //Pesquisa o CEP utilizando webservice ViaCEP
        //Utiliza chamadas ajax recursivas, substituindo os ultimos digitos por zero ate encontrar o endereco
        $.ajax({
            type: "GET",
            url: "//viacep.com.br/ws/"+cep+"/json/?callback=?",
            dataType: 'json', 
            beforeSend: function() {
                $("#form").hide();
                $("#loading").show();
            },
            success: function(endereco) {
                if(!("erro" in endereco))
                {
                    $("#rua").val(endereco.logradouro);
                    $("#bairro").val(endereco.bairro);
                    $("#cidade").val(endereco.localidade);
                    $("#estado").val(endereco.uf);
                    
                    $("#loading").hide();
                    $("#form").show();
                }
                else
                {  
                    cep = cep.split("");
            
                    for(cont = cep.length - 1; cont >= 0; cont--)
                        if(cep[cont] != "0")
                        {
                            cep[cont] = "0";
                            break;
                        }

                    cep = cep.join("");
                    
                    if(cep != "00000000")
                    {
                        $("#cep").val(cep.substr(0, 5) + "-" + cep.substr(5, 3));
                        preenche_form(cep);
                    }
                    else
                    {
                        $("#loading").hide();
                        $("#form").show();
                    
                        $("#resultado").html("<b>CEP inválido!</b>"); 
                    }
                }
            }
        });
    }
    
    function buscar_cep() {
        $("#resultado").html("");       
        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#estado").val("");
        
        var cep = $("#cep").val().trim().replace("-", "");

	if(cep != "")
            preenche_form(cep);
        else
            $("#resultado").html("<b>CEP inválido!</b>");      
    }
    
    function validar()
    {
        if($("#rua").val() != "" && $("#numero").val() != "" && $("#cep").val() != "" && $("#cidade").val() != "" && $("#estado").val() != "")
            return true;
        else 
        {
            $("#resultado").html("<b>Os campos rua, número, cep, cidade e estado são obrigatórios!</b>");
            return false;
        }
    }
</script>
