### Solução Proposta

A solução proposta foi criar uma aplicação web utilizando a estrutura MVC, que contenha: 

- Uma pagina inicial para exibição/consulta e links para a inserção/atualização/exclusão de endereços.
- Uma pagina para inserir endereços.
- Uma pagina para atualizar endereços.
- Uma Servlet para processar as requisições.
- Classes de modelagem.
- Classes de acesso ao banco.
- Banco de dados MySQL.

### View

- index.jsp:
Uma página principal que exibe todos os endereços inseridos, com um input para realização de pesquisa individual e links para a inserção/atualização/exclusão de endereços.

- inserir_endereco.jsp:
Uma pagina que contem um formulário para digitação do endereço a ser inserido. Tem a funcionalidade de pesquisa de cep através de webservice realizada no exercício anterior.  

- alterar_endereco.jsp:
Uma pagina que contem um formulário para digitação dos dados a serem alterados no endereço. Tem a funcionalidade de pesquisa de cep através de webservice realizada no exercício anterior.  

### Model

- Classe Endereço:
Contém atributos relacionados a um endereço e os métodos responsáveis por setar e recuperar os mesmos.

### Controller

- Servlet: 
Classe responsável por receber a requisição e verificar para qual lógica a mesma deve ser redirecionada, de forma a manter o padrão MVC.

- LogicaEndereco
Classe responsável por processar todas as requisições referentes a endereços. Faz a comunicação entre as páginas e a classe de acesso ao banco.

- EnderecoDAO
Classe responsável por fazer o acesso ao banco, realizando a consulta, alteração, inserção e exclusão de endereços.

- AjaxEndereco
Classe responsável por processar as requisições assíncronas dos ajaxs.

- Conexao
Classe responsável por fazer a conexão com o banco de dados MySQL.

### Bibliotecas/API/Tecnologias utilizadas

- Java
- MySQL
- Bootstrap
- JSTL
- JQuery
- Webservice ViaCEP

### Testes

Foi feito um objeto mock para testar as operações de CRUD do projeto.

### Observações Finais

- Projeto criado na IDE Netbeans.
- Foi criado um banco de dados MySQL com uma tabela de endereço.
- O script para criação do banco está salvo na pasta 'controller', com o nome 'crudend_bd.sql'.