/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.crudendereco.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author WallaceRenan
 */
abstract public class ClasseAbstrata {
    
    abstract public void executa(HttpServletRequest req, HttpServletResponse resp);


}
