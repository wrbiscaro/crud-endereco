/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.crudendereco.controller;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
/**
 *
 * @author Wallace Biscaro
 */
public class Conexao {
    private String connectionString;
    private String driver = "com.mysql.jdbc.Driver";
    private String banco = "crudend_bd";
    private String usuario = "root";
    private String senha = "root";
    private String host = "localhost";
    private Connection connection = null;
    public static String status = "Não conectou...";
    
    public Connection conectar() {
        connectionString = "jdbc:mysql://" + host + "/" + banco;

        if (connection == null){
            try {
                Class.forName(driver);
                connection = DriverManager.getConnection(connectionString, usuario, senha);
            }
            catch (Exception ex) {
                System.out.println("Falha na Conexao");
                System.out.println(ex.toString() + ex.getMessage());
            }
        }
        
        //Testa a conexão
        if (connection != null) {
            status = "STATUS = Conectado com sucesso!";
        } 
        else {
            status = "STATUS = Não foi possivel realizar conexão!";
        }

        return connection;
    }

    public String statusConexao() {
         return status;
     }

    public void fecharConexao(){
        if (connection != null){
            try {
                connection.close();
            } 
            catch (SQLException ex) {
                System.out.println(ex.toString());    
            }
        }   
    }
}
