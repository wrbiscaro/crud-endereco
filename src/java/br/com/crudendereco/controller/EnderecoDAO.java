/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.crudendereco.controller;

import br.com.crudendereco.model.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Wallace Biscaro
 */
public class EnderecoDAO {
    private Connection conn;

    public EnderecoDAO(Connection conn) {
        this.conn = conn;
    }  
    
    public boolean inserir(Endereco e) {
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbendereco (rua, numero, cep, cidade, estado, bairro, complemento) VALUES(?,?,?,?,?,?,?)");
            ps.setString(1, e.getRua());
            ps.setString(2, e.getNumero());
            ps.setString(3, e.getCep());
            ps.setString(4, e.getCidade());
            ps.setString(5, e.getEstado());
            ps.setString(6, e.getBairro());
            ps.setString(7, e.getComplemento());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }        
        
        return true;  
    }
    
    public Endereco selecionar(int id){
        Endereco e = null;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbendereco WHERE id = ?");
            ps.setInt(1, id);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){                
                e = new Endereco(rs.getInt("id"), rs.getString("cep"));
                e.setRua(rs.getString("rua"));
                e.setNumero(rs.getString("numero"));                
                e.setCidade(rs.getString("cidade"));
                e.setEstado(rs.getString("estado"));
                e.setBairro(rs.getString("bairro"));
                e.setComplemento(rs.getString("complemento"));
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return e;
    }  
    
    public boolean alterar(Endereco e){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbendereco SET rua = ?, numero = ?, cep = ?, cidade = ?, estado = ?, bairro = ?, complemento = ? WHERE id = ?");
            ps.setString(1, e.getRua());
            ps.setString(2, e.getNumero());
            ps.setString(3, e.getCep());
            ps.setString(4, e.getCidade());
            ps.setString(5, e.getEstado());
            ps.setString(6, e.getBairro());
            ps.setString(7, e.getComplemento());
            ps.setInt(8, e.getId());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }        
        
        return true;  
    }
    
    public boolean excluir(int id){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbendereco WHERE id = ?");
            ps.setInt(1, id);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }  
        
        return true;
    }

    public ArrayList<Endereco> listar(){
        ArrayList<Endereco> enderecos = new ArrayList<>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbendereco");

            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){                
                Endereco e = new Endereco(rs.getInt("id"), rs.getString("cep"));
                e.setRua(rs.getString("rua"));
                e.setNumero(rs.getString("numero"));                
                e.setCidade(rs.getString("cidade"));
                e.setEstado(rs.getString("estado"));
                e.setBairro(rs.getString("bairro"));
                e.setComplemento(rs.getString("complemento"));
                
                enderecos.add(e);
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return enderecos;
    }
}
