/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.crudendereco.controller;

import br.com.crudendereco.model.Endereco;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wallace Biscaro
 */
public class LogicaEndereco extends ClasseAbstrata {
    Conexao conexao = null;
    EnderecoDAO enderecoDao = null;
    Endereco endereco = null;
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        RequestDispatcher rd = null;
        conexao = new Conexao();
        enderecoDao = new EnderecoDAO(conexao.conectar());
        
        String opcao = req.getParameter("Endereco");
        
        if(opcao.equals("Inserir"))
        {
            boolean inserido = this.inserir(req, resp);
            
            if(inserido)
                req.setAttribute("msg", "Endereço inserido com sucesso!");
            else
                req.setAttribute("msg", "Erro ao inserir o endereço! Verifique os dados informados.");           

            rd = req.getRequestDispatcher("inserir_endereco.jsp"); 
        }
        else if(opcao.equals("Alterar"))
        {
            boolean alterado = this.alterar(req, resp);
            
            if(alterado)
                req.setAttribute("msg", "Endereço alterado com sucesso!");
            else
                req.setAttribute("msg", "Erro ao alterar o endereço! Verifique os dados informados.");           

            req.setAttribute("e", endereco);
            rd = req.getRequestDispatcher("alterar_endereco.jsp"); 
        }

        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(LogicaEndereco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogicaEndereco.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    public boolean inserir(HttpServletRequest req, HttpServletResponse resp)
    {
        endereco = new Endereco(0, req.getParameter("cep"));
        endereco.setRua(req.getParameter("rua"));
        endereco.setNumero(req.getParameter("numero"));
        endereco.setCidade(req.getParameter("cidade"));
        endereco.setEstado(req.getParameter("estado"));
        endereco.setBairro(req.getParameter("bairro"));
        endereco.setComplemento(req.getParameter("complemento"));
   
        return enderecoDao.inserir(endereco);
    }
    
    public boolean alterar(HttpServletRequest req, HttpServletResponse resp)
    {
        endereco = new Endereco(Integer.parseInt(req.getParameter("id")), req.getParameter("cep"));
        endereco.setRua(req.getParameter("rua"));
        endereco.setNumero(req.getParameter("numero"));
        endereco.setCidade(req.getParameter("cidade"));
        endereco.setEstado(req.getParameter("estado"));
        endereco.setBairro(req.getParameter("bairro"));
        endereco.setComplemento(req.getParameter("complemento"));
        
        return enderecoDao.alterar(endereco);
    }
}
