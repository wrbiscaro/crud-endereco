/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crudendereco.controller;

import br.com.crudendereco.model.Endereco;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wallace Biscaro
 */
public class AjaxEndereco extends ClasseAbstrata {

    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        Conexao conexao = new Conexao();
        EnderecoDAO enderecoDao = new EnderecoDAO(conexao.conectar());

        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("excluir"))
            {                
                int id = Integer.parseInt(req.getParameter("id"));
                
                boolean delete = enderecoDao.excluir(id);
                
                if(delete) 
                    out.println("Endereço excluído com sucesso!");
                else
                    out.println("Não foi possível excluir o endereço!");  
            }
            else if(acao.equals("localizar"))
            {
                int id = Integer.parseInt(req.getParameter("id"));
                
                Endereco e = enderecoDao.selecionar(id);
                
                if(e == null)
                    out.print("erro");
                else
                    out.print("sucesso"); 
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }     
    }   
}
